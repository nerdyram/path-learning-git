
# Git Eğitimi

## İçerik

- Temel
    - Repo oluşturma(init, clone, config, alias)
    - Değişiklikleri kayıt etme (add, commit, diff, stash, .gitignore)
    - Repo inceleme(status, tag ve blame)
    - Değişiklikleri geri alma (checkout, clean, revert, reset, git rm)
- İleri Seviye
    - Repo'nun tarihcesi yeniden yazma (commit -amend, rebase, rebase -i, reflog, squash)
    - CherryPick, Detached Head
    - Git Flow
    - Hooks
    - Code Freeze
- Soru/Cevap
